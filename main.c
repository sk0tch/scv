#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_exti.h"

void InitBlinkingLed(void) {
  LL_AHB1_GRP1_EnableClock (LL_AHB1_GRP1_PERIPH_GPIOG);
  LL_GPIO_SetPinMode (GPIOG, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
};


void InitButtonControlledLed(void)
{
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_14, LL_GPIO_MODE_OUTPUT);
  LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_INPUT);
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_0);
  LL_EXTI_EnableFallingTrig_0_31(LL_EXTI_LINE_0);
  
  NVIC_EnableIRQ(EXTI0_IRQn);  
}

int main()
{

  InitBlinkingLed();
  InitButtonControlledLed();
  while (1) {
     for(int i = 0; i < 1000000; i++);
      if (LL_GPIO_IsOutputPinSet (GPIOG, LL_GPIO_PIN_13) == 0) {
        LL_GPIO_SetOutputPin (GPIOG, LL_GPIO_PIN_13);
      }
      else {
      LL_GPIO_ResetOutputPin (GPIOG, LL_GPIO_PIN_13);
      }
  }
}


void EXTI0_IRQHandler(void)
{
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
  if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_0) == 1 )
      {
         LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_14);
      }
      else
      {
        LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_14);
      }
}
